const express = require('express');
const cors = require('cors');
const waitPort = require('wait-port');

var mysql = require('mysql'); 

const app = express();

app.use(cors());
app.use(express.json());

require('dotenv').config();

var con = mysql.createConnection({
    host: process.env.MYSQL_HOST,
    user:  process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE
});

waitPort({host: process.env.MYSQL_HOST, port: 3306})
  .then(({ open, ipVersion }) => {
    if (open){
		con.connect(function(err) {
    			if (err) throw err;
    			console.log("request OK");
    			con.query("SELECT * FROM information_schema.tables WHERE TABLE_NAME = 'crew'", function (err, result) {
        			if (err) throw err;
        			if (result.length === 0){
			        	con.query("CREATE TABLE crew (name VARCHAR(255));", function (err, result) {
            			    		if (err) throw err;
             			   		console.log("table 'crew' created")
            				});
        			}
        			else{
       	 	    		console.log("table OK")
        			};
    			});
		});
	}});

app.get('/api', (req, res) => {
    con.query("SELECT * FROM crew", function (err, result) {
        if (err) throw err;
        res.json({'list': result});
    });
});

app.post('/api', (req, res) => {
    con.query("INSERT INTO crew VALUES ('" + req.body["name"] + "')", function (err, result) {
        if (err) throw err;
        console.log("added new crew member " + req.body["name"]);
        res.json({'new': req.body["name"]});
    });
});

app.listen(3111, () => {
    console.log(`Server running on port 3111`);
});
