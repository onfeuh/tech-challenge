import React, { useState, useEffect, useRef } from "react";

function App() {

  const [crewList, setCrewList] = useState([])

  function updateCrewList(){
    fetch("https://wcs.planetai.re/api")
    .then((res) => res.json())
    .then((data) => setCrewList(data.list))
  }

  useEffect(() => {
  	updateCrewList()
  }, []);

  function handleSubmit(){
    updateCrewList()
  }

  return (
    <div className="App">
      <NewMemberForm onSubmit={handleSubmit}/>
      <MemberList list={crewList}/>
    </div>
  );
}

function MemberList({list}){

  return (
    <div>
      <h2>Membres de l'équipage</h2>
      <section className="member-list">
        {list.map(crew =>
        <div className="member-item">{crew.name}</div>
        )}
      </section>
    </div>
  );

}

function NewMemberForm({onSubmit}){

  const ref = useRef("")

  async function handleSubmit(e) {
    e.preventDefault();
    fetch("https://wcs.planetai.re/api", { method: "POST", headers: { 'Content-Type': 'application/json' }, body: JSON.stringify({ 'name': ref.current.value.trim() }) }).then(res => res.json()).then(data => {
      onSubmit();
    })
    ref.current.value = ''
  }

  return (
    <div>
      <h2>Ajouter un(e) Argonaute</h2>
      <form className="new-member-form" onSubmit={handleSubmit}>
        <label htmlFor="name">Nom de l&apos;Argonaute</label>
        <input ref={ref} id="name" name="name" type="text" placeholder="Charalampos" />
        <button type="submit">Envoyer</button>
      </form>
    </div>
  );
}

export default App
